# tinypp

A small agile poker tool. Intended as project to try out vert.x 4 and
event-based programming.

## requirements

- [x] allow creation of lobbies
  - [x] provide lobby name (optional)
  - [x] provide user name
  - [x] provide topic for lobby
- [x] allow joining lobbies via code
  - [x] provide user name
- [x] allow joining lobbies via link
  - [x] provide user name
- [x] offer different presets of cards
- [x] login-less participation
- [x] choose from a set of cards and play one
  - [x] players are informed that someone played a card, but can't see it yet
  - [x] changing vote possible until everyone agreed
- [x] cards are only shown when everyone has voted
  - [x] host (or everone) may get ability to show cards before (to combat AFK participants)
- [x] shown cards can be reset for next round
- [x] statistics about the voting result
- [ ] quota (for creating lobbies)

## current todos / next steps / wip

- [UI] make input fields a bit more convenient (submit on enter etc.)
- [UI] check the flows and error possibilites
- [UI] disable voting buttons after showing
- purge lobbies without participants
- investigate error messages sent by server; are they needed? How else can it be done?

## prod
- [ ] environments / properties
- [ ] SSL handling (http + websocket); HSTS
- [ ] imprint
- [ ] data privacy
- [ ] texts
