package de.mh.tinypp;

import io.vertx.core.Launcher;

public class VertxStarter {
    public static void main(String[] args) {
        Launcher.main(new String[] { "run", "de.mh.tinypp.MainVerticle" });
    }
}
