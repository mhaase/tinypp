package de.mh.tinypp.control;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import de.mh.tinypp.control.bean.CardSet;

public class CardSetRegistry {

    private final List<CardSet> cardSets;

    public CardSetRegistry() {
        cardSets = new LinkedList<>();

        cardSets.add(new CardSet("fib2", "Fibonacci reloaded",
                Arrays.asList("?", "0", "1", "2", "3", "5", "8", "13", "20", "40", "100")));
        cardSets.add(new CardSet("fib1", "Fibonacci",
                Arrays.asList("?", "0", "1", "2", "3", "5", "8", "13", "21", "34", "55", "89")));
        cardSets.add(new CardSet("shirt", "T-Shirt sizes", Arrays.asList("?", "XS", "S", "M", "L", "XL", "XXL")));
    }

    public List<CardSet> getCardSets() {
        return cardSets;
    }

    public Optional<CardSet> getById(String cardSetId) {
        return cardSets.stream().filter(cardSet -> cardSet.getId().equals(cardSetId)).findAny();
    }
}
