package de.mh.tinypp.control;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.regex.Pattern;

import de.mh.tinypp.control.bean.Lobby;
import io.vertx.core.impl.logging.Logger;
import io.vertx.core.impl.logging.LoggerFactory;

// TODO: add scheduler for cleaning up empty lobbies after certain time
public class LobbyManager {

    private final List<Lobby> lobbies;
    //
    private static final String lobbyCharacters = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private static final int LOBBY_ID_LENGTH = 8;
    private static final Pattern lobbyIdValidator = Pattern.compile("[" + lobbyCharacters + "]+");
    //
    private final Logger log = LoggerFactory.getLogger(this.getClass());

    public LobbyManager() {
        lobbies = new LinkedList<>();
    }

    public Lobby createLobby(String id, List<String> cards) {
        String lobbyId = id == null || id.isEmpty() ? generateLobbyId() : id;
        if (lobbyId == null || !lobbyIdValidator.matcher(lobbyId).matches()) {
            log.info("invalid lobbyId provided: [" + lobbyId + "]");
            return null;
        }
        Lobby existingLobby = findLobby(lobbyId);
        if (existingLobby != null) {
            return existingLobby;
        }
        log.info("creating lobby with id [" + lobbyId + "]");
        Lobby lobby = new Lobby(lobbyId, cards);
        lobbies.add(lobby);
        return lobby;
    }

    public Lobby findLobby(String id) {
        for (Lobby lobby : lobbies) {
            if (lobby.getId().equals(id)) {
                return lobby;
            }
        }
        return null;
    }

    private String generateLobbyId() {
        Random random = new Random();

        StringBuffer id = new StringBuffer(LOBBY_ID_LENGTH);
        for (int i = 0; i < LOBBY_ID_LENGTH; i += 1) {
            id.append(lobbyCharacters.charAt(random.nextInt(lobbyCharacters.length())));
        }
        return id.toString();
    }

    public void removeLobby(Lobby lobby) {
        log.info("removing lobby [" + lobby.getId() + "]");
        synchronized (lobbies) {
            lobbies.remove(lobby);
        }
    }
}
