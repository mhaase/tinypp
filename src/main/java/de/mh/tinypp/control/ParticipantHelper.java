package de.mh.tinypp.control;

import java.util.Random;
import java.util.regex.Pattern;

public final class ParticipantHelper {

    private static final Pattern participantNameValidator = Pattern.compile("[^\n\r\t]{2,32}");
    private static final String participantIdCharacters = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private static final int PARTICIPANT_ID_LENGTH = 8;

    public static boolean isValidName(String name) {
        return participantNameValidator.matcher(name).matches();
    }

    public static String generateParticipantId() {
        Random random = new Random();

        StringBuffer id = new StringBuffer(PARTICIPANT_ID_LENGTH);
        for (int i = 0; i < PARTICIPANT_ID_LENGTH; i += 1) {
            id.append(participantIdCharacters.charAt(random.nextInt(participantIdCharacters.length())));
        }
        return id.toString();
    }
}