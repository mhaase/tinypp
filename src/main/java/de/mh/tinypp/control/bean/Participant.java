package de.mh.tinypp.control.bean;

import io.vertx.core.http.ServerWebSocket;

public class Participant {
    private final String id;
    private String name;
    private Lobby lobby;
    private String currentVote;
    private final ServerWebSocket connection;

    public Participant(String id, ServerWebSocket connection) {
        this.id = id;
        this.name = "?";
        this.connection = connection;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Lobby getLobby() {
        return lobby;
    }

    public void setLobby(Lobby lobby) {
        this.lobby = lobby;
    }

    public String getCurrentVote() {
        return currentVote;
    }

    public void setCurrentVote(String currentVote) {
        this.currentVote = currentVote;
    }

    public ServerWebSocket getConnection() {
        return connection;
    }
}
