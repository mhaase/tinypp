package de.mh.tinypp.control.bean;

import java.util.List;

public class CardSet {

    private final String id;
    private final String description;
    private final List<String> allowedCards;

    public CardSet(String id, String description, List<String> allowedCards) {
        this.id = id;
        this.description = description;
        this.allowedCards = allowedCards;
    }

    public String getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public List<String> getAllowedCards() {
        return allowedCards;
    }
}
