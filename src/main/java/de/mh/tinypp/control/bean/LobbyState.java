package de.mh.tinypp.control.bean;

public enum LobbyState {
    VOTING, RESULTS
}
