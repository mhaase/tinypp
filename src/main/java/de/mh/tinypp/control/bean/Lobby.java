package de.mh.tinypp.control.bean;

import java.util.LinkedList;
import java.util.List;

public class Lobby {
    private final String id;
    private LobbyState state;
    private String topic;
    private final List<String> cards;
    private final List<Participant> participants;

    public Lobby(String id, List<String> cards) {
        this.id = id;
        this.state = LobbyState.VOTING;
        this.participants = new LinkedList<>();
        this.cards = cards;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getId() {
        return id;
    }

    public LobbyState getState() {
        return state;
    }

    public void setState(LobbyState state) {
        this.state = state;
    }

    public List<Participant> getParticipants() {
        return participants;
    }

    public List<String> getCards() {
        return cards;
    }
}
