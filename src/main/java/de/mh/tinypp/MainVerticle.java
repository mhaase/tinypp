package de.mh.tinypp;

import de.mh.tinypp.boundary.ApiHandler;
import de.mh.tinypp.boundary.CommunicationHub;
import de.mh.tinypp.control.CardSetRegistry;
import de.mh.tinypp.control.LobbyManager;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.impl.logging.Logger;
import io.vertx.core.impl.logging.LoggerFactory;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.StaticHandler;

public class MainVerticle extends AbstractVerticle {

  private final LobbyManager lobbyManager;
  private final CommunicationHub communicationHub;
  private final ApiHandler apiHandler;
  private final CardSetRegistry cardSetRegistry;

  private final Logger log = LoggerFactory.getLogger(this.getClass());

  public MainVerticle() {
    this.lobbyManager = new LobbyManager();
    this.cardSetRegistry = new CardSetRegistry();
    this.communicationHub = new CommunicationHub(lobbyManager);
    this.apiHandler = new ApiHandler(lobbyManager, cardSetRegistry);
  }

  @Override
  public void start(Promise<Void> startPromise) throws Exception {

    Router router = Router.router(vertx);

    router.route(HttpMethod.GET, "/*").handler(StaticHandler.create());
    router.route(HttpMethod.GET, "/api/card-sets").handler(apiHandler.handleCardSets());
    router.route(HttpMethod.POST, "/api/lobbies").handler(apiHandler.createLobby());

    // TODO: separate websocketHandler and requestHandler into different verticles?
    // What about ports then?
    vertx.createHttpServer().requestHandler(router).webSocketHandler(communicationHub::handle).listen(8888, http -> {
      if (http.succeeded()) {
        startPromise.complete();
        log.info("HTTP server started on port " + http.result().actualPort());
      } else {
        startPromise.fail(http.cause());
      }
    });
  }
}
