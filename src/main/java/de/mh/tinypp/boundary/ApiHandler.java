package de.mh.tinypp.boundary;

import de.mh.tinypp.control.CardSetRegistry;
import de.mh.tinypp.control.LobbyManager;
import de.mh.tinypp.control.bean.Lobby;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;

public class ApiHandler {

    private final LobbyManager lobbyManager;
    private final CardSetRegistry cardSetRegistry;

    public ApiHandler(LobbyManager lobbyManager, CardSetRegistry cardSetRegistry) {
        this.lobbyManager = lobbyManager;
        this.cardSetRegistry = cardSetRegistry;
    }

    public Handler<RoutingContext> handleCardSets() {
        return e -> {
            JsonObject result = new JsonObject();
            result.put("card_sets", cardSetRegistry.getCardSets());
            e.end(result.toString());
        };
    }

    public Handler<RoutingContext> createLobby() {
        return e -> {
            e.request().bodyHandler(bodyBuffer -> {
                JsonObject requestBody = bodyBuffer.toJsonObject();

                String lobbyId = requestBody.getString("lobby_id");
                String cardSetId = requestBody.getString("card_set_id");

                cardSetRegistry.getById(cardSetId).ifPresentOrElse(cardSet -> {
                    Lobby createdLobby = lobbyManager.createLobby(lobbyId, cardSet.getAllowedCards());
                    if (createdLobby == null) {
                        e.end(new JsonObject().put("success", false).put("lobby_id", "could not create lobby")
                                .toString());
                    } else {
                        e.end(new JsonObject().put("success", true).put("lobby_id", createdLobby.getId()).toString());
                    }
                }, () -> {
                    e.end(new JsonObject().put("success", false).put("message", "unknown card set provided")
                            .toString());
                });
            });
        };
    }

}
