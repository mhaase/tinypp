package de.mh.tinypp.boundary;

import java.util.List;

import de.mh.tinypp.control.CardSetRegistry;
import de.mh.tinypp.control.LobbyManager;
import de.mh.tinypp.control.ParticipantHelper;
import de.mh.tinypp.control.bean.Lobby;
import de.mh.tinypp.control.bean.LobbyState;
import de.mh.tinypp.control.bean.Participant;
import io.vertx.core.Handler;
import io.vertx.core.http.ServerWebSocket;
import io.vertx.core.impl.logging.Logger;
import io.vertx.core.impl.logging.LoggerFactory;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

public class CommunicationHub implements Handler<ServerWebSocket> {

    private final LobbyManager lobbyManager;
    //
    private final Logger log = LoggerFactory.getLogger(this.getClass());

    public CommunicationHub(LobbyManager lobbyManager) {
        this.lobbyManager = lobbyManager;
    }

    @Override
    public void handle(ServerWebSocket event) {
        registerParticipant(event);
    }

    private void registerParticipant(ServerWebSocket connection) {
        String id = ParticipantHelper.generateParticipantId();
        Participant participant = new Participant(id, connection);

        connection.textMessageHandler(messageHandler(participant));
        connection.closeHandler(_ignore -> {
            log.info("received close event for participant with name [" + participant.getId() + ", "
                    + participant.getName() + "]");
            if (participant.getLobby() == null) {
                log.info("no lobby found, exiting");
                return;
            }

            // TODO: maybe put in a worker thread? Especially the synchronized block
            Lobby lobby = participant.getLobby();
            log.info("removing participant [" + participant.getId() + ", " + participant.getName() + "] from lobby ["
                    + lobby.getId() + "]");
            List<Participant> participantsOfLobby = lobby.getParticipants();
            synchronized (participantsOfLobby) {
                participantsOfLobby.remove(participant);
            }
            if (lobby.getParticipants().isEmpty()) {
                lobbyManager.removeLobby(lobby);
            } else {
                updateParticipants(lobby);
            }
            participant.setLobby(null);
        });
    }

    private Handler<String> messageHandler(Participant participant) {
        return message -> {
            JsonObject parsedMessage = new JsonObject(message);
            switch (parsedMessage.getString("type")) {
                case "join_lobby" -> {
                    String participantName = parsedMessage.getString("participant_name");
                    if (ParticipantHelper.isValidName(participantName)) {
                        participant.setName(participantName);
                    }
                    Lobby lobby = joinLobby(parsedMessage.getString("lobby_id"));
                    if (lobby == null) {
                        sendLobbyNotFoundMessage(participant);
                        return;
                    }
                    addToLobbyAndAcknowledge(participant, lobby);
                }
                case "update_lobby" -> {
                    Lobby lobby = participant.getLobby();
                    if (lobby == null) {
                        log.warn("trying to update lobby, but participant [" + participant.getId() + ", "
                                + participant.getName() + "] not yet in lobby");
                        return;
                    }
                    lobby.setTopic(parsedMessage.getString("topic"));
                    updateParticipants(lobby);
                }
                case "vote" -> {
                    Lobby lobby = participant.getLobby();
                    if (lobby == null) {
                        log.warn("trying to vote, but participant [" + participant.getId() + ", "
                                + participant.getName() + "] not yet in lobby");
                        return;
                    }
                    String vote = parsedMessage.getString("vote");

                    if (vote == null || !lobby.getCards().contains(vote)) {
                        log.warn("trying to vote, but participant [" + participant.getId() + ", "
                                + participant.getName() + "] casted invalid vote: [" + vote + "]");
                        return;
                    }

                    if (lobby.getState() != LobbyState.VOTING) {
                        log.warn("trying to vote, but lobby had invalid state: [" + lobby.getState() + "]");
                        return;
                    }

                    participant.setCurrentVote(vote);
                    updateCurrentVote(participant);

                    if (lobby.getParticipants().stream().allMatch(p -> p.getCurrentVote() != null)) {
                        // all voted
                        lobby.setState(LobbyState.RESULTS);
                        showVotes(lobby);
                    }
                }
                case "start_over" -> {
                    Lobby lobby = participant.getLobby();
                    if (lobby == null) {
                        log.warn("trying to reset lobby state, but participant [" + participant.getId() + ", "
                                + participant.getName() + "] not yet in lobby");
                        return;
                    }
                    if (lobby.getState() != LobbyState.RESULTS) {
                        log.warn("trying to reset lobby state, but participant [" + participant.getId() + ", "
                                + participant.getName() + "] in lobby with invalid state [" + lobby.getState() + "]");
                        return;
                    }
                    lobby.getParticipants().forEach(p -> p.setCurrentVote(null));
                    lobby.setState(LobbyState.VOTING);
                    updateParticipants(lobby);
                }
                case "show_votes" -> {
                    Lobby lobby = participant.getLobby();
                    if (lobby == null) {
                        log.warn("trying to show votes, but participant [" + participant.getId() + ", "
                                + participant.getName() + "] not yet in lobby");
                        return;
                    }
                    if (lobby.getState() != LobbyState.VOTING) {
                        log.warn("trying to show votes, but participant [" + participant.getId() + ", "
                                + participant.getName() + "] in lobby with invalid state [" + lobby.getState() + "]");
                        return;
                    }
                    lobby.setState(LobbyState.RESULTS);
                    showVotes(lobby);
                }
                default -> {
                    log.info("unknown message type received from participant [" + participant.getId() + ", "
                            + participant.getName() + "]: [" + message + "]");
                }
            }
            ;
        };
    }

    private void addToLobbyAndAcknowledge(Participant p, Lobby l) {
        l.getParticipants().add(p);
        p.setLobby(l);
        greetParticipant(p, l);
    }

    private void greetParticipant(Participant p, Lobby l) {
        JsonObject message = new JsonObject();
        message.put("type", "greetings");
        p.getConnection().writeTextMessage(message.toString(), next -> {
            updateParticipants(l);
        });
    }

    private void updateCurrentVote(Participant p) {
        JsonObject updateMessage = new JsonObject();
        updateMessage.put("type", "update_vote");
        updateMessage.put("id", p.getId());
        updateMessage.put("name", p.getName());
        String message = updateMessage.toString();

        p.getLobby().getParticipants().forEach(parti -> {
            parti.getConnection().writeTextMessage(message);
        });
    }

    private void showVotes(Lobby l) {
        JsonObject updateMessage = new JsonObject();
        updateMessage.put("type", "show_votes");

        JsonArray participants = new JsonArray();
        l.getParticipants().forEach(parti -> {
            JsonObject partiJson = new JsonObject();
            partiJson.put("id", parti.getId());
            partiJson.put("name", parti.getName());
            partiJson.put("vote", parti.getCurrentVote());
            participants.add(partiJson);
        });
        updateMessage.put("participants", participants);

        String message = updateMessage.toString();
        l.getParticipants().forEach(parti -> {
            parti.getConnection().writeTextMessage(message);
        });
    }

    private void updateParticipants(Lobby l) {
        JsonObject updateMessage = new JsonObject();
        updateMessage.put("type", "update_lobby");

        updateMessage.put("lobby_id", l.getId());
        updateMessage.put("topic", l.getTopic());
        updateMessage.put("state", l.getState().name().toLowerCase());
        updateMessage.put("cards", l.getCards());

        JsonArray participants = new JsonArray();
        l.getParticipants().forEach(parti -> {
            JsonObject partiJson = new JsonObject();
            partiJson.put("id", parti.getId());
            partiJson.put("name", parti.getName());
            partiJson.put("voted", parti.getCurrentVote() != null);
            if (l.getState() == LobbyState.RESULTS) {
                partiJson.put("vote", parti.getCurrentVote());
            }
            participants.add(partiJson);
        });
        updateMessage.put("participants", participants);

        String message = updateMessage.toString();
        l.getParticipants().forEach(parti -> {
            parti.getConnection().writeTextMessage(message);
        });
    }

    private Lobby joinLobby(String lobbyId) {
        if (lobbyId != null && !lobbyId.isEmpty()) {
            return lobbyManager.findLobby(lobbyId);
        }
        return null;
    }

    private void sendLobbyNotFoundMessage(Participant participant) {
        JsonObject message = new JsonObject();
        message.put("type", "lobby_not_found");
        participant.getConnection().writeTextMessage(message.toString());
    }
}
