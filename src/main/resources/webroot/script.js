function show(id) {
    byId(id).classList.remove('hidden');
}

function hide(id) {
    byId(id).classList.add('hidden');
}

function disableElement(e) {
    e.disabled = 'disabled';
}
function enableElement(e) {
    e.disabled = '';
}

function clearNode(node) {
    while (node.firstChild) {
        node.removeChild(node.firstChild);
    }
}

function byId(id) {
    return document.getElementById(id);
}